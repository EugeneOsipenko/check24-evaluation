package com.eugeneosipenko.check24evaluation.db

import androidx.room.TypeConverter
import com.eugeneosipenko.check24evaluation.model.Price

object AppTypeConverters {
    @TypeConverter
    @JvmStatic
    fun stringToPrice(data: String): Price {
        val list = data.split("_")
        return Price(list[0].toDouble(), list[1])
    }

    @TypeConverter
    @JvmStatic
    fun priceToString(price: Price): String {
        return "${price.value}_${price.currency}"
    }
}