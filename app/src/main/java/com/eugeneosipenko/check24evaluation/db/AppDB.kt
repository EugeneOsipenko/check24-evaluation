package com.eugeneosipenko.check24evaluation.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.eugeneosipenko.check24evaluation.model.Product

@Database(
    entities = [Product::class],
    exportSchema = false,
    version = 1
)
abstract class AppDB : RoomDatabase() {
    abstract fun productDAO(): ProductDao
}