package com.eugeneosipenko.check24evaluation.ui.productlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.eugeneosipenko.check24evaluation.App
import com.eugeneosipenko.check24evaluation.R
import com.eugeneosipenko.check24evaluation.model.Filter
import com.google.android.material.bottomnavigation.BottomNavigationView
import javax.inject.Inject

class ProductListFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ProductListViewModel by viewModels { viewModelFactory }

    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var productList: RecyclerView
    private lateinit var bottomBar: BottomNavigationView
    private lateinit var progress: View
    private lateinit var adapter: ProductListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeRefreshLayout = view.findViewById(R.id.swipe)
        productList = view.findViewById(R.id.product_list)
        bottomBar = view.findViewById(R.id.bottom_bar)
        progress = view.findViewById(R.id.progress)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        App.component.inject(this)

        productList.layoutManager = LinearLayoutManager(requireContext())
        bottomBar.setOnNavigationItemSelectedListener {
            viewModel.applyFilter(Filter.values()[it.itemId])
            true
        }

        swipeRefreshLayout.setOnRefreshListener {
            setProgressVisible(true)
            bottomBar.menu.clear()
            viewModel.refresh()
        }

        viewModel.products.observe(viewLifecycleOwner) { products ->
            swipeRefreshLayout.isRefreshing = false
            setProgressVisible(false)

            adapter = ProductListAdapter(
                products.header) {
                findNavController(this)
                    .navigate(ProductListFragmentDirections.showDetails(it))
            }

            adapter.data = products.products
            productList.adapter = adapter

            populateBottomBarItems(products.filters)
        }
    }

    private fun populateBottomBarItems(filters: List<Filter>) {
        if (bottomBar.menu.size() > 0) return

        bottomBar.menu.apply {
            clear()
            filters.forEach {
                add(
                    0,
                    it.ordinal,
                    Menu.NONE,
                    it.value
                ).apply { setIcon(R.drawable.ic_filter) }
            }
        }
    }

    private fun setProgressVisible(visible: Boolean) {
        if (visible) {
            progress.visibility = VISIBLE
            progress.alpha = 0f
            progress.animate().alpha(1f)
        } else {
            progress.animate()
                .alpha(0f)
                .withEndAction { progress.visibility = GONE }
        }
    }
}