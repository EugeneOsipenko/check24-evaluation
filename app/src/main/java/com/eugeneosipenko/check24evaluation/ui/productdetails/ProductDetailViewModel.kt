package com.eugeneosipenko.check24evaluation.ui.productdetails

import androidx.lifecycle.ViewModel
import com.eugeneosipenko.check24evaluation.db.AppDB
import javax.inject.Inject

class ProductDetailViewModel @Inject constructor(
    private val db: AppDB
) : ViewModel() {

    fun getProduct(id: Int) = db.productDAO().getProduct(id)
}