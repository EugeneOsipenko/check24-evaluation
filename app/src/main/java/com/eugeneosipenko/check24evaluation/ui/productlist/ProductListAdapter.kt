package com.eugeneosipenko.check24evaluation.ui.productlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.eugeneosipenko.check24evaluation.HeaderFooterAdapter
import com.eugeneosipenko.check24evaluation.R
import com.eugeneosipenko.check24evaluation.model.Header
import com.eugeneosipenko.check24evaluation.model.Product

class ProductListAdapter(
    private val header: Header,
    private val itemClick: (Int) -> Unit
) : HeaderFooterAdapter<Product>(true, true) {

    companion object {
        private const val TYPE_AVAILABLE = 0
        private const val TYPE_UNAVAILABLE = 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder.ItemViewHolder -> holder.bind(getItem(position))
            is ViewHolder.HeaderViewHolder -> holder.bind(header)
            is ViewHolder.FooterViewHolder -> holder.bind()
        }
    }

    override fun getViewType(position: Int) =
        if (getItem(position).available) TYPE_AVAILABLE
        else TYPE_UNAVAILABLE

    override fun getItemView(
        inflater: LayoutInflater, parent: ViewGroup, viewType: Int
    ): RecyclerView.ViewHolder =
        ViewHolder.ItemViewHolder(
            itemClick,
            inflater.inflate(
                when (viewType) {
                    TYPE_AVAILABLE -> R.layout.item_product_available
                    TYPE_UNAVAILABLE -> R.layout.item_product_unavailable
                    else -> throw IllegalArgumentException("unknown view type: $viewType")
                }, parent, false
            )
        )

    override fun getHeaderView(
        inflater: LayoutInflater, parent: ViewGroup
    ): RecyclerView.ViewHolder =
        ViewHolder.HeaderViewHolder(inflater.inflate(R.layout.item_header, parent, false))

    override fun getFooterView(
        inflater: LayoutInflater, parent: ViewGroup
    ): RecyclerView.ViewHolder =
        ViewHolder.FooterViewHolder(inflater.inflate(R.layout.item_footer, parent, false))

    sealed class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        class ItemViewHolder(itemClick: (Int) -> Unit, view: View) : ViewHolder(view) {

            private val name: TextView = view.findViewById(R.id.name)
            private val description: TextView = view.findViewById(R.id.description)
            private val price: TextView? = view.findViewById(R.id.price)
            private val image: ImageView = view.findViewById(R.id.image)
            private val rating: RatingBar = view.findViewById(R.id.rating)

            private var itemId = -1

            init {
                view.setOnClickListener {
                    if (itemId >= 0) {
                        itemClick.invoke(itemId)
                    }
                }
            }

            fun bind(item: Product) {
                name.text = item.name
                description.text = item.description
                price?.text = "${item.price.value} ${item.price.currency}"
                rating.rating = item.rating
                itemId = item.id

                Glide.with(image)
                    .load(item.imageURL)
                    .fallback(R.drawable.image_fallback)
                    .into(image)
            }
        }

        class HeaderViewHolder(view: View) : ViewHolder(view) {

            private val title: TextView = view.findViewById(R.id.title)
            private val description: TextView = view.findViewById(R.id.description)

            fun bind(header: Header) {
                title.text = header.title
                description.text = header.description
            }
        }

        class FooterViewHolder(view: View) : ViewHolder(view) {

            private val footer: TextView = view.findViewById(R.id.title)

            fun bind() {
                this.footer.setText(R.string.footer)
            }
        }
    }
}