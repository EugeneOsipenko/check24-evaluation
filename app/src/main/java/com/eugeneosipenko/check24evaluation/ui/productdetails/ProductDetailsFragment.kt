package com.eugeneosipenko.check24evaluation.ui.productdetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.eugeneosipenko.check24evaluation.App
import com.eugeneosipenko.check24evaluation.R
import javax.inject.Inject

class ProductDetailsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ProductDetailViewModel by viewModels { viewModelFactory }
    private val params by navArgs<ProductDetailsFragmentArgs>()

    private lateinit var name: TextView
    private lateinit var price: TextView
    private lateinit var rating: RatingBar
    private lateinit var image: ImageView
    private lateinit var descriptionShort: TextView
    private lateinit var descriptionFull: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_product_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        name = view.findViewById(R.id.name)
        price = view.findViewById(R.id.price)
        rating = view.findViewById(R.id.rating)
        image = view.findViewById(R.id.image)
        descriptionShort = view.findViewById(R.id.description)
        descriptionFull = view.findViewById(R.id.description_full)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        App.component.inject(this)

        viewModel.getProduct(params.productId).observe(viewLifecycleOwner) { product ->
            name.text = product.name
            descriptionShort.text = product.description
            descriptionFull.text = product.longDescription
            rating.rating = product.rating
            price.text = "${product.price.value} ${product.price.currency}"

            Glide.with(image)
                .load(product.imageURL)
                .into(image)
        }
    }
}