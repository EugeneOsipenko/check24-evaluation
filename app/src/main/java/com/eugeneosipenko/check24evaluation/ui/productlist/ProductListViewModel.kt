package com.eugeneosipenko.check24evaluation.ui.productlist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eugeneosipenko.check24evaluation.db.AppDB
import com.eugeneosipenko.check24evaluation.model.Filter
import com.eugeneosipenko.check24evaluation.model.Header
import com.eugeneosipenko.check24evaluation.model.Product
import com.eugeneosipenko.check24evaluation.model.Products
import com.eugeneosipenko.check24evaluation.repository.ProductRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ProductListViewModel @Inject constructor(
    private val repository: ProductRepository,
    private val db: AppDB
) : ViewModel() {

    val products = MutableLiveData<Products>()

    private val disposable = CompositeDisposable()
    private val items = mutableListOf<Product>()

    private lateinit var filters: List<Filter>
    private lateinit var header: Header

    init {
        loadProducts()
    }

    fun refresh() = loadProducts()

    fun applyFilter(filter: Filter) {
        publishItems(filter)
    }

    private fun loadProducts() {
        disposable.add(
            repository.getProducts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe( { response ->
                    items.clear()
                    items.addAll(response.products)
                    filters = response.filters.map { Filter.fromString(it) }
                    header = response.header

                    db.productDAO().insert(items)
                        .subscribeOn(Schedulers.io())
                        .subscribe()

                    publishItems()
                }, this::onError)
        )
    }

    private fun publishItems(filter: Filter = Filter.ALL) {
        products.value = Products(
            header,
            filters,
            items.filter { filter(it, filter) }
        )
    }

    private fun filter(product: Product, filter: Filter): Boolean {
        return when (filter) {
            Filter.ALL -> true
            Filter.AVAILABLE -> product.available
            Filter.FAVORITES -> product.favorite
        }
    }

    private fun onError(error: Throwable) {
        error.printStackTrace()
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}