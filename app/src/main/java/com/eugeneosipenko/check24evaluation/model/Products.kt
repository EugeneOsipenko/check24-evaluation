package com.eugeneosipenko.check24evaluation.model

data class Products(
    val header: Header,
    val filters: List<Filter>,
    val products: List<Product>
)