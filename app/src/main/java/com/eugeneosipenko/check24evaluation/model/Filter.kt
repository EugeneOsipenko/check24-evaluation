package com.eugeneosipenko.check24evaluation.model

enum class Filter(val value: String) {
    ALL("Alle"),
    AVAILABLE("Verfügbar"),
    FAVORITES("Vorgemerkt");

    companion object {
        fun fromString(value: String): Filter {
            return values()
                .find { it.value == value }
                ?: throw IllegalArgumentException("Unknown filter: $value")
        }
    }
}