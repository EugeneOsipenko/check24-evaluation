package com.eugeneosipenko.check24evaluation.model

import com.google.gson.annotations.SerializedName

data class Header(
    @SerializedName("headerTitle") val title: String,
    @SerializedName("headerDescription") val description: String
)