package com.eugeneosipenko.check24evaluation.model

import androidx.room.Entity
import androidx.room.TypeConverters
import com.eugeneosipenko.check24evaluation.db.AppTypeConverters
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id"])
@TypeConverters(AppTypeConverters::class)
data class Product (
	@SerializedName("name") val name : String,
	@SerializedName("type") val type : String,
	@SerializedName("id") val id : Int,
	@SerializedName("color") val color : String,
	@SerializedName("imageURL") val imageURL : String,
	@SerializedName("colorCode") val colorCode : String,
	@SerializedName("available") val available : Boolean,
	@SerializedName("releaseDate") val releaseDate : Int,
	@SerializedName("description") val description : String,
	@SerializedName("longDescription") val longDescription : String,
	@SerializedName("rating") val rating : Float,
	@SerializedName("price") val price : Price,
	val favorite: Boolean = false
)