package com.eugeneosipenko.check24evaluation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import java.util.Collections.emptyList


abstract class HeaderFooterAdapter<T>(
    private val withHeader: Boolean,
    private val withFooter: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = Int.MIN_VALUE
        private const val TYPE_FOOTER = Int.MIN_VALUE + 1
    }

    var data: List<T> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    protected abstract fun getItemView(
        inflater: LayoutInflater,
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder

    protected abstract fun getHeaderView(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): RecyclerView.ViewHolder?

    protected abstract fun getFooterView(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): RecyclerView.ViewHolder?

    protected abstract fun getViewType(position: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_HEADER -> getHeaderView(inflater, parent) ?: throw RuntimeException("Empty view")
            TYPE_FOOTER -> getFooterView(inflater, parent) ?: throw RuntimeException("Empty view")
            else -> getItemView(inflater, parent, viewType)
        }
    }

    override fun getItemCount(): Int {
        var itemCount = getRealItemCount()
        if (withHeader) itemCount++
        if (withFooter) itemCount++
        return itemCount
    }

    override fun getItemViewType(position: Int): Int =
        if (withHeader && isPositionHeader(position)) TYPE_HEADER
        else if (withFooter && isPositionFooter(position)) TYPE_FOOTER
        else getViewType(position)

    private fun isPositionHeader(position: Int): Boolean = position == 0

    private fun isPositionFooter(position: Int): Boolean = position == itemCount - 1

    protected fun getRealItemCount(): Int = data.size

    protected fun getItem(position: Int): T =
        if (withHeader) data[position - 1] else data[position]

}