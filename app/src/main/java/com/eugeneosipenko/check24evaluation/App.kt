package com.eugeneosipenko.check24evaluation

import android.app.Application
import com.eugeneosipenko.check24evaluation.di.AppComponent
import com.eugeneosipenko.check24evaluation.di.AppModule
import com.eugeneosipenko.check24evaluation.di.DaggerAppComponent


class App : Application() {

    companion object {
        lateinit var daggerComponent: AppComponent

        val component: AppComponent
            get() = daggerComponent
    }

    override fun onCreate() {
        super.onCreate()
        daggerComponent = DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
    }
}
