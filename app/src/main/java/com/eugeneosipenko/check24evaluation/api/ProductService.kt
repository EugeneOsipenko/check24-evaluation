package com.eugeneosipenko.check24evaluation.api

import io.reactivex.Single
import retrofit2.http.GET

interface ProductService {

    @GET("products-test.json")
    fun getProducts(): Single<ProductListResponse>
}