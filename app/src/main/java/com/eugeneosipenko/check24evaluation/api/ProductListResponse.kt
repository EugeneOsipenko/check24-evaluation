package com.eugeneosipenko.check24evaluation.api

import com.eugeneosipenko.check24evaluation.model.Header
import com.eugeneosipenko.check24evaluation.model.Product
import com.google.gson.annotations.SerializedName

data class ProductListResponse(
    @SerializedName("header") val header: Header,
    @SerializedName("filters") val filters: List<String>,
    @SerializedName("products") val products: List<Product>
)