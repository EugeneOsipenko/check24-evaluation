package com.eugeneosipenko.check24evaluation.repository

import com.eugeneosipenko.check24evaluation.api.ProductService

class ProductRepository(
    private val service: ProductService
) {

    fun getProducts() = service.getProducts()
}