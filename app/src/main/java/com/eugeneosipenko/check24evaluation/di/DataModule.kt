package com.eugeneosipenko.check24evaluation.di

import com.eugeneosipenko.check24evaluation.api.ProductService
import com.eugeneosipenko.check24evaluation.repository.ProductRepository
import dagger.Module
import dagger.Provides

@Module
class DataModule {

    @Provides
    fun provideProductRepository(service: ProductService): ProductRepository {
        return ProductRepository(service)
    }
}