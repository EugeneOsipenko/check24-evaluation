package com.eugeneosipenko.check24evaluation.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eugeneosipenko.check24evaluation.ui.productdetails.ProductDetailViewModel
import com.eugeneosipenko.check24evaluation.ui.productlist.ProductListViewModel
import com.eugeneosipenko.check24evaluation.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ProductListViewModel::class)
    abstract fun bindProductListViewModel(viewModel: ProductListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailViewModel::class)
    abstract fun bindProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}
