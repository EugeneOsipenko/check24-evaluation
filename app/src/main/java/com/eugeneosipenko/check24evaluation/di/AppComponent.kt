package com.eugeneosipenko.check24evaluation.di

import com.eugeneosipenko.check24evaluation.ui.productdetails.ProductDetailsFragment
import com.eugeneosipenko.check24evaluation.ui.productlist.ProductListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(fragment: ProductListFragment)
    fun inject(fragment: ProductDetailsFragment)
}
