package com.eugeneosipenko.check24evaluation.di

import android.app.Application
import androidx.room.Room
import com.eugeneosipenko.check24evaluation.api.ProductService
import com.eugeneosipenko.check24evaluation.db.AppDB
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, DataModule::class])
class AppModule(
    val app: Application
) {

    @Singleton
    @Provides
    fun provideProductService(): ProductService {
        return Retrofit.Builder()
            .baseUrl("https://app.check24.de/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ProductService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(): AppDB {
        return Room
            .databaseBuilder(app, AppDB::class.java, "app.db")
            .fallbackToDestructiveMigration()
            .build()
    }
}
